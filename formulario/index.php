<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Cabin+Condensed:400,500,600,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
<script src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
		
				function valida_envia(){
					
					if(document.fvalida.nombre.value.length==0){
						alert("Por Favor escriba su nombre");
						document.fvalida.nombre.focus();
						return false;
					}

          if(document.fvalida.apellidos.value.length==0){
            alert("Por Favor escriba su Apellido");
            document.fvalida.apellidos.focus();
            return false;
          }          

          if(document.fvalida.cedula.value.length==0){
            alert("Por Favor escriba su cedula");
            document.fvalida.cedula.focus();
            return false;
          }          
					
					if(document.fvalida.email.value.indexOf('@')==-1){
					alert("La direccion de email no es correcta");
					document.fvalida.email.focus();
					return false;
					}
					
					if(document.fvalida.telefono.value.length==0){
						alert("Por Favor escriba su telefono");
						document.fvalida.telefono.focus();
						return false;
					}

          if(document.fvalida.celular.value.length==0){
            alert("Por Favor escriba su celular");
            document.fvalida.celular.focus();
            return false;
          }          
					
			
					document.fvalida.submit();
					
					
				}
</script>

<style>



* {
background:none;
}

.error {
    color: #CC0000;
}
em {
    color: #CC0000;
}
input[type="text"] {
    background: #EFEFEF;
    border: none;
    color: #4D4D4D;
    font-family: 'Roboto Slab', serif;
    font-size: 18px;
    padding: 10px 0;
    padding-left: 23px;
    width: 80%;
    margin-top: 5px;
}

input[type="submit"] {
    font-family: 'Roboto Slab', serif;
    font-size: 18px;
    text-align: center;
    width: 87%;
    border: none;
    background: #00B1DC;
    color: #FFFFFF;
    margin: 35px 0;
    padding: 7px 0;
}

input[type="reset"] {
}

input::-webkit-input-placeholder {
  color: #808080;
}
input:-moz-placeholder {
  color: #808080; 
}
input:-ms-input-placeholder { 
  color: #808080; 
}

h2{
    font-family: 'Comfortaa', cursive;
    font-size: 23px;
    background-color: #007CBB;
    border-bottom: 2px solid #005D8D;
    color: #FFFFFF;
    font-weight: 100;
    width: 100%;
    text-align: center;
    padding: 15px 0;
}
h2 span{
    width: 162px;
    display: inline-block;
    text-align: left;
    margin-left: 59px;
    position: relative;
}
h2 span:before{
    content: '';
    position: absolute;
    left: -69px;
    top: 0;
    background-image: url(formulario.png);
    background-repeat: no-repeat;
    background-position: center;
    width: 60px;
    height: 60px;
    background-color: #A4D9EC;
    border-radius: 50%;
}
h2 strong{
    font-weight: 100;
    font-size: 27px;
}
.contacto-programas{
    text-align: center;
    border: 1px solid #ccc;
    padding-top: 31px;
}


#commentForm label.error {
    margin-left: 6px;
}

@media only screen and (max-width: 480px) and ( min-width:0px ) {
}

</style>

<body>
<form id="commentForm"  name="fvalida" method="post" action="procesar.php" onSubmit="return valida_envia()">
    <h2><span>Formulario <strong>de contacto</strong></span></h2>
<table class="contacto-programas" width="100%" border="0">
  <tr>
    <td><input name="nombre" placeholder="Nombre" type="text" id="cname" class="required" /></td>
  </tr>
  <tr>
    <td><input name="apellidos" placeholder="Apellidos" type="text" id="cname" class="required" /></td>
  </tr>
  <tr>
    <td><input name="cedula" placeholder="Cedula" type="text" id="cedula" class="required" /></td>
  </tr>
  <tr>
    <td><input name="telefono" placeholder="Telefono fijo" type="text" id="telefono" class="required" /></td>
  </tr>
  <tr>
    <td><input name="celular" placeholder="Celular" type="text" id="movil" class="required" /></td>
  </tr>
  <tr>
    <td><input name="email" placeholder="Email" type="text" id="cemail" class="required" /></td>
  </tr>
  <tr>
    <td><input name="programa"  type="text" id="programa" class="required" value= "<?php echo $_GET ['programa'];?>" readonly /></td>
  </tr>
   <tr>
    <td><input type="submit" name="Submit" value="ENVIAR" /></td>
  </tr>
</table>	
</form>

