 /**
* Menu JS
*
*/
( function( $, window, document ){

	$(document).ready(function(){

		$(window).stellar();
		
		// Despliega menu principal en resoluciones inferiores
		$( "#trigger" ).click(function() {

			event.preventDefault();

			if( $( ".menu-up" ).is( ':visible' ) )
				$( ".menu-up" ).slideUp( 400 );
			else
				$( ".menu-up" ).slideDown( 400 );

		});

		$( "#trigger-2" ).click(function() {

			event.preventDefault();

			if( $( "#flyout_menu_92" ).is( ':visible' ) )
				$( "#flyout_menu_92" ).slideUp( 400 );
			else
				$( "#flyout_menu_92" ).slideDown( 400 );

		});

		$( "#trigger-3" ).click(function(event) {

			event.preventDefault();

			if( $( ".acceso" ).is( ':visible' ) )
				$( ".acceso" ).slideUp( 400 );
			else
				$( ".acceso" ).slideDown( 400 );

		});

	});
	

})( jQuery, this, this.document, undefined );