/**
* Vista de los programas en el home
*
*/
( function( $, window, document ){

	var ProgramasView = function( a ){

	};

	ProgramasView.prototype = {

		/**
		* Initialize the view
		*
		*/
		initialize: function(){

			this.goToProgramas();
			this.goToHeader();
			this.showPrograma();
		},

		/**
		* Go to the second slide
		*
		*/
		goToProgramas: function(){


			$( '.go-to-programas' ).click( function( e ){

				e.preventDefault();

				$('html, body').animate({
			        scrollTop: $("#section-two").offset().top
			    }, 800);

			});
		},

		goToHeader: function(){


			$( '.go-to-header' ).click( function( e ){

				e.preventDefault();

				$('html, body').animate({
			        scrollTop: $("#section-one").offset().top
			    }, 800);

			});
		},

		/**
		* Displays the programas submenu and shows the first carousel
		*
		*/
		showPrograma: function(){


			$( '.menu-programas a' ).click( function( e ){

				e.preventDefault();

				$( '.menu-programas a' ).parent().removeClass( 'active' );
				$( this ).parent().addClass( 'active' );
				$( '.item-menus > ul' ).hide();
				$( '.content-slider-programas .moduletable' ).hide();

				var submenu = $( this ).text().toLowerCase();

				var firstItem = $( '.' + submenu ).find( 'a:first-child' ).attr( 'href' ).replace( '#', '' );
				$( '.' + submenu ).find( 'li' ).removeClass( 'active' );
				$( '.' + submenu ).find( 'li:first-child' ).addClass( 'active' );
				$( '.' + firstItem ).fadeIn();
				
				$( '.' + submenu ).fadeIn();
			});

			$( '.submenu-programas a' ).click( function( e ){

				e.preventDefault();

				$( '.submenu-programas li' ).removeClass( 'active' );
				$( '.content-slider-programas .moduletable' ).hide();

				var slider = $( this ).attr( 'href' ).replace( '#', '' );

				$( this ).parent().addClass( 'active' );
				$( '.' + slider ).fadeIn();

				return;
			});
		}

	}

	window.ProgramasView = new ProgramasView();
	window.ProgramasView.initialize();
})( jQuery, this, this.document, undefined );