<?php
/**
* @version 1.0.0
* @package RSSearch! 1.0.0
* @copyright (C) 2011 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');
class JFormFieldK2categories extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'K2categories';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		JFactory::getLanguage()->load('com_rssearch', JPATH_ADMINISTRATOR);
		
		if (file_exists(JPATH_SITE.'/components/com_k2/k2.php')) {
			$db = JFactory::getDBO();
			$db->setQuery("SELECT * FROM #__k2_categories ORDER BY `name` ASC");
			$categories = $db->loadObjectList();

			$result = JHTML::_('select.genericlist', $categories, $this->name.'[]', 'multiple="multiple"', 'id', 'name', $this->value);
		} else $result = JText::_('RSS_K2_INSTALL_ERROR');
		
		return $result;
	}
}