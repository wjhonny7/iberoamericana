<?php
/**
* @version 1.0.0
* @package RSSearch! 1.0.0
* @copyright (C) 2011 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.plugin.plugin');

class plgRSSearchK2 extends JPlugin
{
	public function __construct( &$subject, $config ) {
		parent::__construct( $subject, $config );
	}
	
	public function getResults(&$results, $search, $params) {
	    $db			= JFactory::getDBO();
	    $fields		= (array)$this->params->get('searchfields','');
	    $categories = (array) $this->params->get('k2categories','');
	    $orderby	= $this->params->get('orderby','title');
	    $asc_desc	= $this->params->get('asc_desc','ASC');
	    $itemId		= $this->params->get('Itemid','');
	    $itemId		= (empty($itemId) ? '' : '&Itemid='.$itemId);

		JFactory::getLanguage()->load('plg_rssearch_k2',JPATH_ADMINISTRATOR);
		require_once JPATH_ADMINISTRATOR . '/components/com_search/helpers/search.php';

		foreach($fields as $field) {
			if ($field == 'k2_items') {
				$tablecolumns	= array('title','hits','ordering','created');
				$where_catid	= '';
				
				if (!in_array('', $categories)) {
					JArrayHelper::toInteger($categories);
					$where_catid = ' AND '.$db->qn('catid').' IN ('.implode(',', $categories).') ';
				}
				
				$query = 'SELECT '.$db->qn('id').', '.$db->qn('title').', '.$db->qn('alias').', '.$db->qn('catid').', '.$db->qn('introtext').' FROM '.$db->qn('#__k2_items').' WHERE ('.$db->qn('title').' LIKE '.$db->q('%'.$search.'%').' OR '.$db->qn('introtext').' LIKE '.$db->q('%'.$search.'%').' OR '.$db->qn('fulltext').' LIKE '.$db->q('%'.$search.'%').') AND '.$db->qn('published').' = 1 '.$where_catid.' GROUP BY '.$db->qn('id').'';

				if (in_array($orderby,$tablecolumns)) 
					$query .= ' ORDER BY '.$db->qn($orderby).' '.$db->escape($asc_desc);
				
				$db->setQuery($query);
				if ($list = $db->loadObjectList()) {
					foreach ($list as $item) {
						if ( !searchHelper::checkNoHTML($item, $search, array('title', 'introtext','fulltext')) ) continue;

						$tmp		= new stdClass();
						$tmp->title = $item->title;
						$tmp->link 	= JRoute::_('index.php?option=com_k2&view=item&id='.$item->id.':'.$item->alias.''.$itemId);
						$tmp->text 	= $item->introtext;
						$tmp->type 	= JText::_('RSF_K2_XML_ARTICLE');
						$results[] 	= $tmp;
					}
				}
			} elseif($field == 'k2_categories') {
				$where_id = '';
				
				if (!in_array('', $categories)) {
					JArrayHelper::toInteger($categories);
					$where_id = ' AND '.$db->qn('id').' IN ('.implode(',', $categories).') ';
				}
				
				$query = 'SELECT '.$db->qn('id').', '.$db->qn('name').', '.$db->qn('alias').', '.$db->qn('description').' FROM '.$db->qn('#__k2_categories').' WHERE ('.$db->qn('name').' LIKE '.$db->q('%'.$search.'%').' OR '.$db->qn('description').' LIKE '.$db->q('%'.$search.'%').') AND '.$db->qn('published').' = 1 '.$where_id.' GROUP BY '.$db->qn('id').', '.$db->qn('name').', '.$db->qn('description').'';

				if ($orderby == 'ordering')
					$query .= '	ORDER BY '.$db->qn('ordering').' '.$db->escape($asc_desc);
				
				$db->setQuery($query);
				if ($list = $db->loadObjectList()) {
					foreach ($list as $item) {
						if ( !searchHelper::checkNoHTML($item, $search, array('name','description')) ) continue;

						$tmp		= new stdClass();
						$tmp->title = $item->name;
						$tmp->link 	= JRoute::_('index.php?option=com_k2&view=itemlist&task=category&id='.$item->id.':'.$item->alias.$itemId);
						$tmp->text 	= $item->description;
						$tmp->type 	= JText::_('RSF_K2_XML_CATEGORY');
						$results[] 	= $tmp;
					}
				}
			}
		}
	    return true;
	}
}