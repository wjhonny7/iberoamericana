<?php
/**
* @version 1.0.0
* @package RSSearch! 1.0.0
* @copyright (C) 2011 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.plugin.plugin');

class plgRSSearchVirtuemart extends JPlugin
{
	public function __construct( &$subject, $config ) {
		parent::__construct( $subject, $config );
	}
	
	public function getResults(&$results, $search, $params) {
	    $db 					= JFactory::getDBO();
		$lang 					= JFactory::getLanguage();
		$available_languages 	= $lang->getKnownLanguages();
		$tag					= $lang->getTag();

	    $fields 		= (array) $this->params->get('searchfields','');
	    $categories 	= (array) $this->params->get('virtuemartcategories','');
	    $orderby 		= $this->params->get('orderby','');
	    $asc_desc 		= $this->params->get('asc_desc','ASC');
	    $itemId 		= $this->params->get('Itemid','');
	    $itemId 		= (empty($itemId) ? '' : '&Itemid='.$itemId);
		
		JFactory::getLanguage()->load('plg_rssearch_virtuemart',JPATH_ADMINISTRATOR);

		require_once JPATH_ADMINISTRATOR . '/components/com_search/helpers/search.php';

		foreach($fields as $field) {
			if($field == 'products') {
				$tablecolumns = array('product_name');
				$where_catid  = '';

				$db->setQuery("SHOW TABLES LIKE '".$db->getPrefix()."virtuemart_products%'");
				$lang_tables = $db->loadColumn();

				if (!in_array('', $categories))
					$where_catid = ' AND '.$db->qn('c.virtuemart_category_id').' IN ('.implode(',', $categories).') ';
					
				foreach($available_languages as $language) {
					if ($tag != $language['tag']) continue;
					$suffix = strtolower(str_replace('-','_', $language['tag']));

					if (!empty($lang_tables) && in_array($db->getPrefix().'virtuemart_products_'.$suffix, $lang_tables)) {
						$query = 'SELECT '.$db->qn('p.virtuemart_product_id').', '.$db->qn('p.product_name').', '.$db->qn('p.product_desc').', '.$db->qn('p.product_s_desc').', '.$db->qn('c.virtuemart_category_id').', '.$db->qn('ps.published').' FROM '.$db->qn('#__virtuemart_products_'.$suffix,'p').' LEFT JOIN '.$db->qn('#__virtuemart_product_categories','c').' ON '.$db->qn('p.virtuemart_product_id').' = '.$db->qn('c.virtuemart_product_id').' LEFT JOIN '.$db->qn('#__virtuemart_products','ps').' ON '.$db->qn('p.virtuemart_product_id').' = '.$db->qn('ps.virtuemart_product_id').' WHERE ('.$db->qn('p.product_name').' LIKE '.$db->q('%'.$search.'%').' OR '.$db->qn('p.product_s_desc').' LIKE '.$db->q('%'.$search.'%').' OR '.$db->qn('p.product_desc').' LIKE '.$db->q('%'.$search.'%').') AND '.$db->qn('ps.published').' = '.$db->q('1').' '.$where_catid.' GROUP BY '.$db->qn('p.virtuemart_product_id');
						
						if (in_array($orderby,$tablecolumns))
							$query .= ' ORDER BY '.$db->qn('p.'.$orderby).' '.$db->escape($asc_desc);
						
						$db->setQuery($query);
						if ($products = $db->loadObjectList()) {
							foreach ($products as $item) {
								if ( !searchHelper::checkNoHTML($item, $search, array('product_name', 'product_s_desc', 'product_desc')) ) continue;

								$tmp		= new stdClass();
								$tmp->title = $item->product_name;
								$tmp->link 	= JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$item->virtuemart_product_id.'&virtuemart_category_id='.$item->virtuemart_category_id.$itemId.'&lang='.substr($suffix, 0, strpos($suffix, '_')));
								$tmp->text 	= $item->product_s_desc;
								$tmp->type 	= JText::_('RSS_VM_XML_PRODUCT');
								$results[] 	= $tmp;
							}
						}
					}
				}
			} elseif($field == 'categories'){
				$tablecolumns 	= array('ordering','category_name');
				$where_catid 	= '';

				$db->setQuery("SHOW TABLES LIKE '".$db->getPrefix()."virtuemart_categories%'");
				$lang_tables = $db->loadColumn();
				
				if(!in_array('', $categories))
					$where_catid = ' AND '.$db->qn('cl.virtuemart_category_id').' IN ('.implode(',', $categories).') ';
				
				foreach($available_languages as $language) {
					if ($tag != $language['tag']) continue;
					$suffix = strtolower(str_replace('-','_', $language['tag']));

					if(!empty($lang_tables) && in_array($db->getPrefix().'virtuemart_categories_'.$suffix, $lang_tables)) {
						$query = 'SELECT '.$db->qn('cl.virtuemart_category_id').', '.$db->qn('cl.category_name').', '.$db->qn('cl.category_description').' FROM '.$db->qn('#__virtuemart_categories_'.$suffix,'cl').' LEFT JOIN '.$db->qn('#__virtuemart_categories','c').' ON '.$db->qn('c.virtuemart_category_id').' = '.$db->qn('cl.virtuemart_category_id').' WHERE ('.$db->qn('cl.category_name').' LIKE '.$db->q('%'.$search.'%').' OR '.$db->qn('cl.category_description').' LIKE '.$db->q('%'.$search.'%').') AND '.$db->qn('c.published').' = 1 '.$where_catid;
							
						if (in_array($orderby,$tablecolumns))
							$query .= 'ORDER BY '.$db->escape($orderby).' '.$db->escape($asc_desc);
						
						$db->setQuery($query);
						if ($cats = $db->loadObjectList()) {
							foreach ($cats as $item) {
								if ( !searchHelper::checkNoHTML($item, $search, array('category_name', 'category_description')) ) continue;

								$tmp 		= new stdClass();
								$tmp->title = $item->category_name;
								$tmp->link  = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$item->virtuemart_category_id.$itemId.'&lang='.substr($suffix, 0, strpos($suffix, '_')));
								$tmp->text  = $item->category_description;
								$tmp->type 	= JText::_('RSS_VM_XML_CATEGORY');
								$results[]  = $tmp;
							}
						}
					}
				}
			}
		}
	    return true;
	}
}