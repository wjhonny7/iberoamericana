<?php
/**
* @version 1.0.0
* @package RSSearch! 1.0.0
* @copyright (C) 2011 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');

class JFormFieldVirtuemartcategories extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Virtuemartcategories';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput() {
		JFactory::getLanguage()->load('com_rssearch', JPATH_ADMINISTRATOR);
		
		$db 					= JFactory::getDBO();
		$lang 					= JFactory::getLanguage();
		$available_languages 	= $lang->getKnownLanguages();

		$categories_tables 	= array();
		$categories			= array();

		$db->setQuery("SHOW TABLES LIKE '".$db->getPrefix()."virtuemart_categories%'");
		$table_lang = $db->loadColumn();

		foreach($available_languages as $language) {
			$suffix = strtolower(str_replace('-','_', $language['tag']));
			
			if(!empty($table_lang) && in_array($db->getPrefix().'virtuemart_categories_'.$suffix, $table_lang)) {
				$db->setQuery("SELECT cl.virtuemart_category_id, cl.category_name FROM #__virtuemart_categories_".$suffix." AS cl LEFT JOIN #__virtuemart_categories AS c ON c.virtuemart_category_id = cl.virtuemart_category_id WHERE c.published = 1 ORDER BY virtuemart_category_id ASC");
				$db_categories = $db->loadObjectList();

				foreach($db_categories as $cat) {
					if (array_key_exists($cat->virtuemart_category_id, $categories))	$categories[$cat->virtuemart_category_id] .= ' / '.$cat->category_name;
					else $categories[$cat->virtuemart_category_id] = $cat->category_name;
				}
			}
		}

		return JHTML::_('select.genericlist', $categories, $this->name.'[]', 'multiple="multiple"', 'value', 'text', $this->value);
	}
}