<?php
/**
 * @package		JCalPro
 * @subpackage	plg_system_jcalpro

**********************************************
JCal Pro
Copyright (c) 2006-2012 Anything-Digital.com
**********************************************
JCalPro is a native Joomla! calendar component for Joomla!

JCal Pro was once a fork of the existing Extcalendar component for Joomla!
(com_extcal_0_9_2_RC4.zip from mamboguru.com).
Extcal (http://sourceforge.net/projects/extcal) was renamed
and adapted to become a Mambo/Joomla! component by
Matthew Friedman, and further modified by David McKinnis
(mamboguru.com) to repair some security holes.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This header must not be removed. Additional contributions/changes
may be added to this header as long as no information is deleted.
**********************************************
Get the latest version of JCal Pro at:
http://anything-digital.com/
**********************************************

 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.filesystem.file');
jimport('joomla.plugin.plugin');
// we HAVE to force-load the helper here to prevent fatal errors!
$helper = JPATH_ADMINISTRATOR . '/components/com_jcalpro/helpers/jcalpro.php';
if (JFile::exists($helper)) require_once $helper;

class plgSystemJCalPro extends JPlugin
{
	public static $com = 'com_jcalpro';
	
	private static $_run;
	
	private static $_debug;
	
	/**
	 * Constructor
	 * 
	 * @param unknown_type $subject
	 * @param unknown_type $config
	 */
	public function __construct(&$subject, $config) {
		// if something happens & the helper class can't be found, we don't want a fatal error here
		if (class_exists('JCalPro')) {
			JCalPro::language(JCalPro::COM, JPATH_ADMINISTRATOR);
			self::$_run = true;
		}
		else {
			$this->loadLanguage();
			JFactory::getApplication()->enqueueMessage(JText::_('PLG_SYSTEM_JCALPRO_COMPONENT_NOT_INSTALLED'));
			self::$_run = false;
		}
		parent::__construct($subject, $config);
	}
	
	public function loadLanguage($extension = 'plg_system_jcalpro.sys', $basePath = JPATH_ADMINISTRATOR) {
		parent::loadLanguage($extension, $basePath);
	}
	
	public function onAfterInitialise() {
		if (!self::$_run) return;
		// for debugging only :)
		if (defined('JDEBUG') && JDEBUG && $this->params->get('debug', true)) {
			try {
				jimport('jcaldate.date');
				$time = new JCalDate();
				$this->loadLanguage();
				$this->_debug  = JText::sprintf('PLG_SYSTEM_JCALPRO_DEBUG_TIMES', $time->toSql(), $time->toJoomla()->toSql(), $time->timezone(), $time->toUser()->toSql(), $time->timezone());
			}
			catch (Exception $e) {
				return;
			}
		}
	}
	
	/**
	 * onAfterDispatch
	 * 
	 * handles flair after dispatch
	 */
	public function onAfterDispatch() {
		if (!self::$_run) return;
		$app = JFactory::getApplication();
		if (!empty($this->_debug) && 'component' != $app->input->get('tmpl')) {
			$app->enqueueMessage($this->_debug);
		}
		// we want to add some extras to com_categories
		if ($app->isAdmin() && 'com_categories' == $app->input->get('option', '', 'cmd') && JCalPro::COM == $app->input->get('extension', '', 'cmd') && class_exists('JCalPro')) {
			// UPDATE: don't do this in edit layout in 3.0+
			if (JCalPro::version()->isCompatible('3.0') && 'edit' == $app->input->get('layout')) return;
			// add submenu to categories
			JLoader::register('JCalProView', JPATH_ADMINISTRATOR . '/components/' . JCalPro::COM . '/libraries/views/baseview.php');
			$comView = new JCalProView();
			$comView->addMenuBar();
			// add script to inject extra columns into the categories list table
			JCalPro::registerHelper('url');
			JText::script('COM_JCALPRO_TOTAL_EVENTS');
			JText::script('COM_JCALPRO_UPCOMING_EVENTS');
			JFactory::getDocument()->addScript(JCalProHelperUrl::media() . '/js/jcalpro.js');
			JFactory::getDocument()->addScript(JCalProHelperUrl::media() . '/js/categories.js');
		}
	}
	
	/**
	 * onGetIcons
	 * 
	 * @param string $context
	 * @return array
	 */
	public function onGetIcons($context) {
		if (!self::$_run) return;
		if ('mod_quickicon' == $context && class_exists('JCalPro') && $this->params->get('quickicon', true)) {
			// get our helper
			JCalPro::registerHelper('url');
			// link to the main page
			return array(array(
				'link' => JCalProHelperUrl::_()
			,	'text' => JText::_(JCalPro::COM)
			,	'access' => JCalPro::version()->isCompatible('3.0') ? JFactory::getUser()->authorise('core.manage', JCalPro::COM) : array('core.manage', JCalPro::COM)
			));
		}
		return array();
	}
	
	public function getDebug() {
		return $this->_debug;
	}
}
