<?php
/**
 * @package Component jVoteSystem for Joomla! 1.5 - 2.5
 * @projectsite www.joomess.de/projects/jvotesystem
 * @authors Johannes Meßmer, Andreas Fischer
 * @copyright (C) 2010 - 2012 Johannes Meßmer
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

//-- No direct access
defined('_JEXEC') or die('=;)');

jimport('joomla.plugin.plugin');

class plgSystemjVoteSystemDatabase extends JPlugin
{

    function plgSystemjVoteSystemDatabase( &$subject, $config )
    {
        parent::__construct( $subject, $config );

    }//function
    
    public function onBeforeCompileHead() {
    	// Make sure jVoteSystem is installed
    	if(!file_exists(JPATH_ADMINISTRATOR.'/components/com_jvotesystem')) {
    		return;
    	}
    	
    	//Save logs
    	require_once JPATH_SITE.DS.'components'.DS.'com_jvotesystem'.DS.'classes'.DS.'loader.php';
    	$log =& VBLog::getInstance();
    	 
    	$log->save();
    	
    	$db = JFactory::getDBO();
    	
    	// Is jVoteSystem enabled?
    	if(version_compare(JVERSION, '1.6.0', 'ge')) {
    		$db->setQuery('SELECT `enabled` FROM `#__extensions` WHERE `element` = "com_jvotesystem" AND `type` = "component"');
    		$enabled = $db->loadResult();
    	} else {
    		$db->setQuery('SELECT `enabled` FROM `#__components` WHERE `link` = "option=com_jvotesystem"');
    		$enabled = $db->loadResult();
    	}
    	if(!$enabled) return;
    	
    	//Small fixes after new Installation -> check it only in the backend
    	$app = JFactory::getApplication();
    	if($app->isAdmin() && JRequest::getString("option","") == "com_installer") {
    		if(!version_compare( JVERSION, '1.6.0', 'lt' )) {
    			require_once JPATH_SITE.DS.'components'.DS.'com_jvotesystem'.DS.'classes'.DS.'loader.php';
    			$log =& VBLog::getInstance();
    	
    			//Wenn Menüeintrag nicht angezeigt wird, reparieren
    			$db->setQuery('SELECT `extension_id` FROM `#__extensions` WHERE `element` = "com_jvotesystem" AND `type` = "component" AND `protected`=0');
    			$id = $db->loadResult();
    	
    			if($id) {
    				$sql = 'UPDATE `#__menu`
    				SET `component_id`="'.$id.'"
    				WHERE `menutype` = "main"
    				AND `client_id` = 1
    				AND `alias` = "jvotesystem"';
    				$db->setQuery($sql);
    				$db->query();
    					
    				if($db->getErrorMsg()) $log->add("ERROR", "FailedToRepairAdminMenu", array ('db_error'=>$db->getErrorMsg()));
    				if($db->getAffectedRows() > 0) $log->add("DB", "RepairedAdminMenu");
    			}
    		}
    	}
    	
    	// Use a 20% chance of running; this allows multiple concurrent page
    	$random = rand(1, 5);
    	if($random != 3) return;
    	
    	// Do we have to run (at most once per 1 day)?
    	jimport('joomla.html.parameter');
    	jimport('joomla.application.component.helper');
    	$component =& JComponentHelper::getComponent('com_jvotesystem');
    	$params = new JParameter($component->params);
    	$last = $params->getValue('plg_jvotesystemdatabase', 0);
    	$now = time();
    	if(abs($now-$last) < 10800) return;
    	
    	// Update last run status
    	$params->setValue('plg_jvotesystemdatabase', $now);
    	$db = JFactory::getDBO();
    	if( version_compare(JVERSION,'1.6.0','ge') ) {
    		// Joomla! 1.6
    		$data = $params->toString('JSON');
    		$db->setQuery('UPDATE `#__extensions` SET `params` = '.$db->Quote($data).' WHERE '.
    				"`element` = ".$db->quote('com_jvotesystem')." AND `type` = 'component'");
    	} else {
    		// Joomla! 1.5
    		$data = $params->toString('INI');
    		$db->setQuery('UPDATE `#__components` SET `params` = '.$db->Quote($data).' WHERE '.
    				"`option` = ".$db->quote('com_jvotesystem')." AND `parent` = 0 AND `menuid` = 0");
    	}
    	
    	// If a DB error occurs, return null
    	if(version_compare(JVERSION, '1.6.0', 'ge')) {
    		try {
    			$db->query();
    		} catch (Exception $e) {
    			return;
    		}
    	} else {
    		$db->query();
    		if($db->getErrorNum()) return;
    	}
    	
    	require_once JPATH_SITE.DS.'components'.DS.'com_jvotesystem'.DS.'classes'.DS.'loader.php';
    	$vbparams =& VBParams::getInstance();
    	
    	//Check for an update -> email notification
    	$updater =& VBUpdate::getInstance();
    	if($updater->needUpdate()) {
    		$data = $updater->getServerData();
    			
    		$lib =& joomessLibrary::getInstance();
    		if($lib->getParam('jvs_update_email_notification', "") != $data->version) {
    			VBMail::getInstance()->updateNotification($data->version, $data->download, $data->changelog);
    			$lib->setParam('jvs_update_email_notification', $data->version, true);
    		}
    	}
    	
    	//Delete email tasks
    	$tasksPeriod =(int)$vbparams->get("validityPeriod");
    	$db->setQuery("	DELETE FROM `#__jvotesystem_email_tasks`
    			WHERE DATE_SUB(CURDATE(),INTERVAL ".($tasksPeriod*2)." DAY) > `created` ");
    	$db->query();
    	if(@$db->getAffectedRows() > 0)
    		$log->add("DB",	"CleanedUpEmailTasks", array("rows" => $db->getAffectedRows(), "period" => $tasksPeriod*2));
    	if($db->getErrorMsg()) $log->add("ERROR",	"CleanUpEmailTasksFailed", array("db_error" => $db->getErrorMsg()));
    	
    	//Delete Session-Table after x-Days
    	$keepSessionData = $vbparams->get("sessionDataPeriod");
    	$db->setQuery("	DELETE FROM `#__jvotesystem_sessions`
    			WHERE DATE_SUB(CURDATE(),INTERVAL ".$keepSessionData." DAY) > `lastVisitDate` ");
    	$db->query();
    	if(@$db->getAffectedRows() > 0)
    		$log->add("DB",	"CleanedUpSessionData", array("rows" => $db->getAffectedRows(), "period" => $keepSessionData));
    	if($db->getErrorMsg()) $log->add("ERROR",	"CleanUpSessionFailed", array("db_error" => $db->getErrorMsg()));
    	
    	//Allow only 5 session entries per user
    	$sql = "SELECT s.`user_id`, COUNT(s.`id`) AS counts
    	FROM `#__jvotesystem_sessions` AS s
    	GROUP BY s.`user_id`
    	HAVING counts > 5
    	ORDER BY `counts` DESC ";
    	$db->setQuery($sql);
    	$counts = $db->loadObjectList();
    	
    	$logs = array();
    	foreach($counts AS $count) {
    		$db->setQuery("	DELETE FROM `#__jvotesystem_sessions`
    				WHERE `user_id`='".$count->user_id."'
    				ORDER BY `lastVisitDate` ASC
    				LIMIT ".($count->counts - 5));
    		$db->query();
    		if(@$db->getAffectedRows() > 0)
    			$logs[] = array("rows" => $db->getAffectedRows(), "uid" => $count->user_id);
    		if($db->getErrorMsg()) $log->add("ERROR",	"CleanUpUserSessionFailed", array("db_error" => $db->getErrorMsg(), "uid" => $count->user_id));
    	}
    	if(!empty($logs))
    		$log->add("DB",	"CleanedUpUserSessionData", $logs);
			
    	$log->save();
    }
}//class
