function iFrameHeight(iframeName) {
	if(document.getElementById && !(document.all)) {
		h = document.getElementById(iframeName).contentDocument.body.offsetHeight;
		document.getElementById(iframeName).height = h;
	} else if(document.all) {
		h = document.frames(iframeName).document.body.scrollHeight;
		document.all[iframeName].height = h;
	}
}