<?php
    $url = JFactory::getURI()->root();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta content="telephone=no" name="format-detection">

        <!-- Joomla Head -->
        <jdoc:include type="head" />

        <!-- Fonts-->
        
        <!-- Less -->
        <link href='http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/onepcssgrid.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/reset.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=template"/>

        <script type="text/javascript">
            var url = "<?php echo $url; ?>";
        </script>

        <!-- Script -->
        <script src="js/jquery.stellar.min.js"></script>

    </head>

    <body>
        <jdoc:include type="message" /> 
        <!--Primera section-->
        <div class="matriculate">
            <a href="index.php/aspirantes"><img src="images/page-eight/matriculate.png"/></a>
        </div>
        <div class="section page-one home" id="section-one">
            <jdoc:include type="modules" name="background" style="xhtml" /> 
            <header>
                <div class="onepcssgrid-1200">
                    <div class="onerow header">
                        <div class="col3">
                            <jdoc:include type="modules" name="buscador" style="xhtml" /> 
                        </div>
                        <div class="col9 last">
                            <a id="trigger" href="#">Perfiles</a>
                            <jdoc:include type="modules" name="menu-up" style="xhtml" /> 
                        </div>
                    </div>
                    <div class="onerow header-logo">
                        <div class="col4">
                            <jdoc:include type="modules" name="logo" style="xhtml" /> 
                        </div>
                        <div class="col3 rds">
                            <jdoc:include type="modules" name="redes" style="xhtml" /> 
                        </div>
                        <div class="col5 last">
                            <jdoc:include type="modules" name="chat" style="xhtml" /> 
                        </div>
                    </div>
                </div>
            </header>
            <nav>
                <div class="onepcssgrid-1200">
                    <div class="onerow">
                        <div class="col12">
                            <a id="trigger-2" href="#">Menú Principal</a>
                            <jdoc:include type="modules" name="menu-principal" style="xhtml" /> 
                        </div>
                    </div>
                </div>
            </nav>
            <main>
                <jdoc:include type="modules" name="slider-header" style="xhtml" />
            </main>

            <div class="down">
                <a href="#" class="go-to-programas"><img src="images/page-one/FlechaBajar.png"/></a>
            </div>
        </div>

        <!--Segunda section-->

        <div class="section page-two" id="section-two">
            <div class="onepcssgrid-1200">
                <div class="onerow">
                    <div class="col12">
                        <jdoc:include type="modules" name="menu-programas" style="xhtml" />
                    </div>
                </div>

                <div class="onerow">
                    <div class="col2">
                        <jdoc:include type="modules" name="item-menu-programas" style="xhtml" />
                    </div>

                    <div class="col10 content-slider-programas last">
                        <jdoc:include type="modules" name="slider-programas" style="xhtml" />
                    </div>
                </div>  
            </div>
        </div>

        <!--Tercera section-->

        <div data-stellar-background-ratio="0.5" class="section page-three">

            <div class="menu-lateral">
                <jdoc:include type="modules" name="menu-lateral" style="xhtml" />
            </div>
            <div class="menu-lateral-2">
                <ul>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div>
            <div class="onepcssgrid-1200">
                <div class="onerow">
                    <div class="col12">
                        <div class="content-acceso">
                            <a id="trigger-3" href="#">Accesos rápidos</a>
                            <jdoc:include type="modules" name="acceso-rapido" style="xhtml" />
                        </div>
                    </div>
                </div>

                <div class="onerow">
                    <div class="col12">
                        <jdoc:include type="modules" name="noticias" style="xhtml" />
                    </div>
                </div>

                <div class="onerow">
                    <div class="col12 last">
                        <jdoc:include type="modules" name="investigacion" style="xhtml" />
                    </div>
                </div>  
            </div>
        </div>

        <!--Cuarta section-->

        <div class="section page-four">
            <div class="onepcssgrid-1200">
                <div class="onerow">
                    <div class="col12">
                        <jdoc:include type="modules" name="calendario" style="xhtml" />
                    </div>
                </div> 
            </div>
        </div>

        <!--Quinta section-->

        <div data-stellar-background-ratio="0.5" class="section page-five">
            <div class="onepcssgrid-1200">
                <div class="onerow">
                    <div class="col6">
                        <jdoc:include type="modules" name="twitter" style="xhtml" />
                    </div>
                    <div class="col6 last">
                        <jdoc:include type="modules" name="facebook" style="xhtml" />
                    </div>
                </div> 
            </div>
        </div>

        <!--Sexta section-->

        <div class="section page-six">
            <div class="wrapper">
                <div class="onepcssgrid-1200">
                    <div class="onerow">
                        <div class="col12">
                            <jdoc:include type="modules" name="slider" style="xhtml" />
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <!--Septima section-->

        <div class="section page-seven">
            <div class="onepcssgrid-1200">
                <div class="onerow">
                    <div class="col3">
                        <jdoc:include type="modules" name="campus" style="xhtml" />
                    </div>
                    <div class="col3">
                        <jdoc:include type="modules" name="campus-mapa" style="xhtml" />
                    </div>
                    <div class="col3">
                        <jdoc:include type="modules" name="videos" style="xhtml" />
                    </div>
                    <div class="col3 last">
                        <jdoc:include type="modules" name="encuesta" style="xhtml" />
                    </div>
                </div> 
            </div>
        </div>

        <!--Octava section-->

        <div class="section page-eight">
            <div class="onepcssgrid-1200">
                <div class="onerow">
                    <div class="col6">
                        <jdoc:include type="modules" name="redes-pie" style="xhtml" />
                    </div>
                    <div class="col6 last like">
                        <jdoc:include type="modules" name="me-gusta" style="xhtml" />
                    </div>
                </div> 
            </div>
        </div>

        <!--Novena section-->

        <div class="section page-nine">
            <div class="onepcssgrid-1200">
                <div class="onerow">
                    <div class="col12">
                        <jdoc:include type="modules" name="pie" style="xhtml" />
                    </div>
                </div>
            </div>
            <div class="down">
                <a href="#" class="go-to-header"><img src="images/page-eight/subir.png"/></a>
            </div>
        </div>
        <div id="copy"><span class="">&copy 2014 | Sitio Web Desarrollado Por</span>
            <a target="_blank" href="http://www.creandopaginasweb.com/">
                <div id="logo">
                </div>
            </a>
        </div>

        <!--PIE DE LA PAGINA-->

        <script src="js/menu.js"></script>
        <script src="js/programas.js"></script>

        <!-- SCRIPT DE CRAZY EGG PARA MAPAS DE CALOR. Borrar cuando se termine el estudio. -->
        <script type="text/javascript">
        setTimeout(function(){var a=document.createElement("script");
        var b=document.getElementsByTagName("script")[0];
        a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0028/3755.js?"+Math.floor(new Date().getTime()/3600000);
        a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
        </script>
    </body>
</html>