<?php

/*********************************************************************************
* LiveZilla groups.inc.php
* 
* Copyright 2011 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
* 
* Improper changes in this file may cause critical errors.
* To modify LiveZilla Server settings it is strongly recommended to use 
* LiveZilla Server Admin application and desist from editing this file directly.
* 
********************************************************************************/ 

$GROUPS = Array();
$GROUPS["chat"] = Array();
$GROUPS["chat"]["gr_desc"] = "YToxOntzOjI6IkVOIjtzOjg6IlkyaGhkQT09Ijt9";
$GROUPS["chat"]["gr_external"] = 1;
$GROUPS["chat"]["gr_internal"] = 1;
$GROUPS["chat"]["gr_created"] = "1424349718";
$GROUPS["chat"]["gr_email"] = "chat@iberoamericana.edu.co";
$GROUPS["chat"]["gr_standard"] = 1;
$GROUPS["chat"]["gr_vfilters"] = "YTowOnt9";
$GROUPS["chat"]["gr_hours"] = array();
$GROUPS["chat"]["gr_ex_sm"] = "1";
$GROUPS["chat"]["gr_ex_so"] = "1";
$GROUPS["chat"]["gr_ex_pr"] = "1";
$GROUPS["chat"]["gr_ex_ra"] = "1";
$GROUPS["chat"]["gr_ex_fv"] = "0";
$GROUPS["chat"]["gr_ex_fu"] = "1";
$GROUPS["chat"]["gr_ci_hidden"] = Array();
$GROUPS["chat"]["gr_ti_hidden"] = Array();
$GROUPS["chat"]["gr_ci_mand"] = Array();
$GROUPS["chat"]["gr_ti_mand"] = Array();
?>