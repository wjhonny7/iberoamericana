<?php

/*********************************************************************************
* LiveZilla internal.inc.php
* 
* Copyright 2011 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
* 
* Improper changes in this file may cause critical errors.
* To modify LiveZilla Server settings it is strongly recommended to use 
* LiveZilla Server Admin application and desist from editing this file directly.
* 
********************************************************************************/ 

$INTERN = Array();
$INTERN["31c7166"] = Array();
$INTERN["31c7166"]["in_id"] = "administrator";
$INTERN["31c7166"]["in_level"] = "1";
$INTERN["31c7166"]["in_groups"] = "YToxOntpOjA7czo4OiJZMmhoZEE9PSI7fQ==";
$INTERN["31c7166"]["in_name"] = "admin";
$INTERN["31c7166"]["in_desc"] = "";
$INTERN["31c7166"]["in_email"] = "info@iberoamericana.edu.co";
$INTERN["31c7166"]["in_websp"] = 5;
$INTERN["31c7166"]["in_perms"] = "2222222";
$INTERN["31c7166"]["in_lang"] = "ES";
$INTERN["31c7166"]["in_aac"] = "1";
$INTERN["31c7166"]["in_lipr"] = "";
?>