/**
*
* ObjectModel for { Object }
*
*/

( function( $, window, document, Utilities ){

	var ObjectModel = function( a ){

	};

	ObjectModel.prototype = {


			/**
			* Triggers a method in backend, sending data and returning the response
			*
			*/
			myMethod: function( success, _data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_mycomponent"
					,	task: "controller.action"
					,   data: _data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
				
			}

	};

	// Expose to global scope
	window.ObjectModel = new ObjectModel();

})( jQuery, this, this.document, this.Misc, undefined );