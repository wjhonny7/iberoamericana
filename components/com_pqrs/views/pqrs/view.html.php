<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class PqrsViewPqrs extends JView {
	// Function that initializes the view
	function display( $tpl = null ){
	
		// Add the toolbar with the CRUD modules defined
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules

}
?>