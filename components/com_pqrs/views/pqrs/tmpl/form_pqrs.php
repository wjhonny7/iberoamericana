<div class="wrapper-form-pqrs">

	<table class="content_form" border=0>
		<tr>
			<td>
				<label>Nombres y Apellidos: </label>
				<input type="text" name="nombre" >
			</td>
			<td>
				<label>Email:</label>
				<input type="text" name="email" >
			</td>
		</tr>
		<tr>
			<td>
				<label>Documento de identidad;</label>
				<input type="text" name="identificacion" >
			</td>
			<td>
				<label>Solicitante</label>
				<select id="solicitante" name="solicitante">
				<option value =""></option>	
				<option value ="Colaborador">Colaborador</option>
				<option value ="Estudiante">Estudiante</option>
				<option value ="Egresado">Egresado</option>
				<option value ="Docente">Docente</option>
				<option value ="Aspirante">Aspirante</option>
				<option value ="Proveedor">Proveedor</option>
				</select>
			</td>						
		</tr>
		<tr>
			<td>
				<ul class="tels">
					<li>
						<label>Teléfono</label>
						<input type="text" name="telefono" >
					</li>
					<li>
						<label>Celular</label>
						<input type="text" name="celular" >
					</li>						
				</ul>
		
			</td>
			<td>
				<label>Título o tema</label>
				<input type="text" name="tema">
			</td>						
		</tr>
		<tr>
			<td colspan="2">
				<label>Mensaje</label>
				<textarea name="mensaje" class="mensaje"></textarea>
			</td>					
		</tr>
	</table>	
</div>	