<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Anuncios Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_si
 */
class PqrsController extends JControllerLegacy {
	/**
	 * @var		string	The default view.
	 * @since	1.6
	 */
	protected $default_view = 'pqrs';

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display( $cachable = false, $urlparams = false) {

		// $language = JFactory::getLanguage();

		// $lang = JRequest::getVar('lang');

		// if ($lang == 'es'):
		// 	$language->load('com_usuarios', JPATH_SITE, 'es-ES', true);
		// else:
		// 	$language->load('com_usuarios', JPATH_SITE, 'en-GB', true);
		// endif;
		

		parent::display( $cachable, $urlparams );

		return $this;
	}
}