#############################################
##          Events Table - Metadata        ##
#############################################;

ALTER TABLE #__jcalpro_events ADD
	`metadata` TEXT NOT NULL default ''
	COMMENT 'JSON encoded metadata.'
	AFTER `params`
;
